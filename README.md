Custom SELinux policies
-----------------------

A collection of custom policies to make applications I use SELinux-compatible.

**DISCLAIMER:** I am _not_ an expert SELinux policy developer. While the 
policies look fine to me, they might not be more permissive than needed, and/or 
introduce security holes by overextending the permissions found in the reference 
policy. Use at your own risk.
