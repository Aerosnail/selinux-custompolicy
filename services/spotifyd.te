policy_module(spotifyd, 1.0)

#########################
# 
# Declarations
# 

attribute_role spotifyd_roles;
role spotifyd_roles types spotifyd_t;

type spotifyd_t;
type spotifyd_exec_t;
init_daemon_domain(spotifyd_t, spotifyd_exec_t)
userdom_user_application_domain(spotifyd_t, spotifyd_exec_t)

type spotifyd_xdg_config_t;
files_config_file(spotifyd_xdg_config_t)

type spotifyd_xdg_cache_t;
files_runtime_file(spotifyd_xdg_cache_t)

type spotifyd_tmp_t;
files_tmp_file(spotifyd_tmp_t)

type spotifyd_port_t;
corenet_port(spotifyd_port_t);

#########################
#
# Local policy
#

allow spotifyd_t self:process { getsched setsched };
allow spotifyd_t self:fifo_file { manage_file_perms };
allow spotifyd_t self:unix_dgram_socket { create ioctl connect };
allow spotifyd_t self:tcp_socket { listen };
allow spotifyd_t spotifyd_port_t:tcp_socket { name_connect };
allow spotifyd_t spotifyd_port_t:udp_socket { name_bind };

corenet_tcp_bind_generic_node(spotifyd_t)
corenet_udp_bind_generic_node(spotifyd_t)

# Allow connecting to spotify
sysnet_dns_name_resolve(spotifyd_t)
corenet_udp_bind_generic_node(spotifyd_t)
corenet_tcp_connect_http_port(spotifyd_t)

# Allow printing status info
term_use_all_terms(spotifyd_t)

# Allow connection to pipewire sound server 
optional_policy(`
	pipewire_client_domain(spotifyd_t)
	pulseaudio_client_domain(spotifyd_t)
')

# Allow access to TLS certificates (HTTPS support)
miscfiles_read_generic_certs(spotifyd_t)

# Allow access to alsa resources (needed for alsa backend)
alsa_read_config(spotifyd_t)
files_search_var_lib(spotifyd_t)

# Allow connection to dbus (for pulseaudio)
optional_policy(`
	dbus_all_session_bus_client(spotifyd_t)
	dbus_connect_all_session_bus(spotifyd_t)
')

# Allow access to config files in home dir
xdg_read_config_home_files(spotifyd_t)
xdg_search_config_home_dirs(spotifyd_t)
manage_dirs_pattern(spotifyd_t, spotifyd_xdg_config_t, spotifyd_xdg_config_t)
manage_files_pattern(spotifyd_t, spotifyd_xdg_config_t, spotifyd_xdg_config_t)
xdg_config_home_filetrans(spotifyd_t, spotifyd_xdg_config_t, dir, "spotifyd")
xdg_config_home_filetrans(spotifyd_t, spotifyd_xdg_config_t, file)

# Allow saving settings to cache
xdg_read_cache_home_files(spotifyd_t)
xdg_search_cache_dirs(spotifyd_t)
manage_dirs_pattern(spotifyd_t, spotifyd_xdg_cache_t, spotifyd_xdg_cache_t)
manage_files_pattern(spotifyd_t, spotifyd_xdg_cache_t, spotifyd_xdg_cache_t)
xdg_cache_filetrans(spotifyd_t, spotifyd_xdg_cache_t, dir)
xdg_cache_filetrans(spotifyd_t, spotifyd_xdg_cache_t, file)

# Allow creating temporary files
files_search_tmp(spotifyd_t)
userdom_list_user_tmp(spotifyd_t)
manage_dirs_pattern(spotifyd_t, spotifyd_tmp_t, spotifyd_tmp_t)
manage_files_pattern(spotifyd_t, spotifyd_tmp_t, spotifyd_tmp_t)
files_tmp_filetrans(spotifyd_t, spotifyd_tmp_t, file)

# Apparently needed for pulseaudio backend to work properly
gen_require(`
	type tmpfs_t;
')
allow spotifyd_t tmpfs_t:file { map };
fs_rw_tmpfs_files(spotifyd_t)

# Allow sending output to syslog when run without --no-daemon flag
logging_send_syslog_msg(spotifyd_t)


files_dontaudit_read_etc_files(spotifyd_t)
files_dontaudit_getattr_all_files(spotifyd_t)
